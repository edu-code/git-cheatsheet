## Первоначальная настройка
### Представляемся
- `git config --global user.name "Your Name"`
- `git config --global user.email "you@example.com"`

Если сертификат сервера самоподписный, нужно отключить проверку сертификатов https. Это часто происходит с серверов внутри предприятия, если есть свой удостоверяющий центр.

`git config --global http.sslVerify false` - Это отключит проверку сертификатов https. Иначе при клонировании по https получим ошибку Peer's Certificate issuer is not recognized.

### Генерация ключей
- `ssh-keygen -q -t rsa -N "$SUDO_USER" -f ~/.ssh/id_rsa` - Создаём SSH ключ
- `ssh-keygen -y -f ~/.ssh/id_rsa` - Выводим публичную часть ключа
Копируем ее и добавляем публичный ключ в профиль VCS, чтобы она ассоциировала нашу учетную запись с приватным ключом.
** По требованиям безопасности приватный ключ не должен покидать машину, на которой был сгенерирован в не зашифрованном виде **

### Клонируем репозиторий
- `git clone git@localhost:project/repository.git --depth 1 <repositoty_name>_local_copy/` - склонирует все ветки <branch_name>, только последний коммит в папку <repositoty_name>_local_copy
- `git clone git@localhost:project/repository.git --branch <branch_name> --depth 1 <repositoty_name>_local_copy/` - склонирует только ветку <branch_name>, только последний коммит в папку <repositoty_name>_local_copy
- `ssh-agent bash -c 'ssh-add /path/to/key; git clone -b <branch_name> --single-branch git@localhost:project/repository.git --depth 1 <repositoty_name>_local_copy'` - клонирование с указанием ключа для авторизации

## Использование каждый день
Перед началом работы выполняем получение обновлений с центрального репозитория коммандой `git pull`. Команда представляет собой объединение команд `git fetch` и `git merge`.
```
git pull
```
Для изменений создается новая ветка, работа ведется в ней, после окончания она сливается в основную ветку. Создайте новую ветку %тип_изменения%-%краткое_описание_изменений%, например feature-add-healthcheck. Тип изменения обычно feature или bugfix. Вместо типа изменения может указываться номер тикета в issue tracking системе такой как [Atlassian Jira](https://www.atlassian.com/software/jira). Ребята не обманываю, когда пишут "The #1 software development tool used by agile teams" система очень популярна в больших компаниях.
```
git checkout -b add-healthcheck
```
После внесения изменений проверяем, что git видит только нужные файлы. Если есть что-то лишнее - можно добавить в файл .gitignore **TODO где про него рассказать** или сделать добавление в stage по одному файлу.
```
git status
```
добавляем файлы в stage
```
git add README.md # Добавляем только файл README.md
git add --all # Добавляем все измененныи и неотслеживаемые файлы
```
проверяем, что все добавилось правильно
```
git status
```
Перед коммитом выполняем снова получение обновлений с центрального репозитория коммандой `git pull`
Коммитим изменения в локальный репозиторий
```
git commit -m "Added new route /health"
```
Отправляем изменения на удаленный сервер
```
git push -u origin feature-add-healthcheck
```
